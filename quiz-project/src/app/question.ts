import { Option } from './option';

export class Question {
    id: number;
    question: string;
    options: Option[];

    constructor(data: any) {
        this.id =  data.id;
        this.question = data.question;
        this.options = [];
        data.options.forEach(optionResponse => {
            var newOption = new Option(optionResponse);
            this.options.push(newOption);
        })
    }
}
